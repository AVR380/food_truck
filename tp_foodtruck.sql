-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 31 mai 2019 à 14:45
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp_foodtruck`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualite`
--

DROP TABLE IF EXISTS `actualite`;
CREATE TABLE IF NOT EXISTS `actualite` (
  `id_actualite` int(11) NOT NULL,
  `promotion` varchar(45) DEFAULT NULL,
  `articles_conseilles` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_actualite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id_article` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `prix_ht` double NOT NULL,
  `stock` int(11) NOT NULL,
  `Type` char(1) NOT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article_has_repas`
--

DROP TABLE IF EXISTS `article_has_repas`;
CREATE TABLE IF NOT EXISTS `article_has_repas` (
  `Article_id_article` int(11) NOT NULL,
  `Repas_id_repas` int(11) NOT NULL,
  `quentite_article` varchar(45) NOT NULL,
  PRIMARY KEY (`Article_id_article`,`Repas_id_repas`),
  KEY `fk_Article_has_Repas_Repas1` (`Repas_id_repas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id_commande` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `statut` varchar(45) NOT NULL,
  `date_de_commande` datetime NOT NULL,
  `adresse_facturation` varchar(45) NOT NULL,
  `adresse_livraison` varchar(45) NOT NULL,
  `date_livraison` date DEFAULT NULL,
  `statut_livraison` varchar(45) NOT NULL,
  `commentaire` text,
  PRIMARY KEY (`id_commande`,`id_utilisateur`),
  KEY `fk_Commande_Client1` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ligne`
--

DROP TABLE IF EXISTS `ligne`;
CREATE TABLE IF NOT EXISTS `ligne` (
  `id_ligne` int(11) NOT NULL,
  `Commande_id_commande` int(11) NOT NULL,
  `prix_unitaire` tinyint(4) NOT NULL,
  `quantite_repas` int(11) NOT NULL,
  PRIMARY KEY (`id_ligne`,`Commande_id_commande`),
  KEY `fk_Ligne_Commande1` (`Commande_id_commande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `repas`
--

DROP TABLE IF EXISTS `repas`;
CREATE TABLE IF NOT EXISTS `repas` (
  `id_repas` int(11) NOT NULL,
  `Ligne_id_ligne` int(11) NOT NULL,
  PRIMARY KEY (`id_repas`,`Ligne_id_ligne`),
  KEY `fk_Repas_Ligne1` (`Ligne_id_ligne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `statut` varchar(45) NOT NULL,
  `date_de_naissance` varchar(45) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `mot_de_passe` varchar(45) NOT NULL,
  `societe` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `genre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article_has_repas`
--
ALTER TABLE `article_has_repas`
  ADD CONSTRAINT `fk_Article_has_Repas_Article` FOREIGN KEY (`Article_id_article`) REFERENCES `article` (`id_article`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Article_has_Repas_Repas1` FOREIGN KEY (`Repas_id_repas`) REFERENCES `repas` (`id_repas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_Commande_Client1` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `ligne`
--
ALTER TABLE `ligne`
  ADD CONSTRAINT `fk_Ligne_Commande1` FOREIGN KEY (`Commande_id_commande`) REFERENCES `commande` (`id_commande`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `repas`
--
ALTER TABLE `repas`
  ADD CONSTRAINT `fk_Repas_Ligne1` FOREIGN KEY (`Ligne_id_ligne`) REFERENCES `ligne` (`id_ligne`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
